from bookticket.objects.bus_station import BusStation


def main(bs):
    while True:
        try:
            choice = int(input("MENU:\n"
                               "0 - exit\n"
                               "1 - add new bus\n"
                               "2 - add new route\n"
                               "3 - customer service\n"
                               "4 - list of buses\n"
                               "5 - list of routes\n"
                               "Make your choice: "))
        except ValueError:
            print("Enter a digit value")
            continue
        match choice:
            case 0:
                print("SEE YOU SOON!")
                break
            case 1:
                bus_station.set_buses()
            case 2:
                bus_station.set_routes()
            case 3:
                bus_station.customer_service()
            case 4:
                bus_station.show_buses()
            case 5:
                bus_station.show_routes()
            case _:
                print("Wrong choice")


if __name__ == '__main__':
    bus_station = BusStation()
    main(bus_station)
