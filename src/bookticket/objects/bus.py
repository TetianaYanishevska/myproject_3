class Bus:

    def __init__(self, bus_id, model, license_plate, number_of_seats):
        self.bus_id = bus_id
        self.model = model
        self.license_plate = license_plate
        self.number_of_seats = number_of_seats

    def __str__(self):
        return f"Model: {self.model} License_plate: {self.license_plate}"
