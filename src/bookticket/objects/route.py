class Route:

    def __init__(self, route_id, date, time, route, bus_id):
        self.route_id = route_id
        self.date = date
        self.time = time
        self.route = route
        self.bus_id = bus_id

    def __str__(self):
        return f"Route ID: {self.route_id}, Date: {self.date}, Time: {self.time}, Route: {self.route}"


