class Ticket:

    def __init__(self, ticket_num, route_id, seat, uuid):
        self.ticket_num = ticket_num
        self.route_id = route_id
        self.seat = seat
        self.uuid = uuid

    def __str__(self):
        return f"{self.ticket_num} Route {self.route_id} Seat {self.seat} Unique Identifier {self.uuid}"
