import uuid
import re
import os
import random
from datetime import datetime
from tabulate import tabulate
from bookticket.objects.route import Route
from bookticket.objects.ticket import Ticket
from bookticket.objects.work_with_file import WorkWithFile


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class TicketError(Exception):
    def __init__(self, message):
        self.message = message


class BusStation(metaclass=SingletonMeta):

    def __init__(self):
        self.work_with_file_buses = WorkWithFile(os.sep.join([os.getcwd(), 'buses.csv']),
                                                 ['bus_id', 'model', 'license_plate', 'number_of_seats'])
        self.work_with_file_routes = WorkWithFile(os.sep.join([os.getcwd(), 'routes.csv']),
                                                  ['route_id', 'date', 'time', 'route', 'bus_id'])
        self.work_with_file_available_seats = WorkWithFile(os.sep.join([os.getcwd(), 'available_seats.csv']),
                                                           ['route_id', 'seats'])
        self.work_with_file_sold_tickets = WorkWithFile(os.sep.join([os.getcwd(), 'sold_tickets.csv']),
                                                        ['ticket_num', 'route_id', 'seat', 'uuid'])
        self.routes = []
        self.available_seats = []
        self.sold_tickets = []

    def set_buses(self):
        # Додаємо автобуси до бази даних автобусів
        # -------------------------------------------------------------------
        self.work_with_file_buses.check_file()
        bus_id = input("Bus ID: ")
        model = input("Model: ")
        license_plate = input("License_plate: ")
        number_of_seats = int(input("Number_of_seats: "))
        bus = {'bus_id': bus_id, 'model': model, 'license_plate': license_plate, 'number_of_seats': number_of_seats}
        self.work_with_file_buses.write_file(bus)

    def set_routes(self):
        # Додаємо маршрути до бази даних маршрутів
        # Формуємо перелік вільних місць для продажу на маршрут, який додається
        # ---------------------------------------------------------------------
        if not os.path.exists(os.sep.join([os.getcwd(), 'buses.csv'])) or len(self.work_with_file_buses.read_file()) == 0:
            print("You don't have any bus to assign to the route. You should first create a database of buses")
            return
        self.work_with_file_routes.check_file()
        route_id = int(input("Route ID: "))
        date = input("Date in DD-MM-YYYY format: ")
        time = input("Time in HH:MM format: ")
        route = input("Route in FROM-TO format: ")
        bus_id = input("Bus ID: ")
        route = {'route_id': route_id, 'date': date, 'time': time, 'route': route, 'bus_id': bus_id}
        self.work_with_file_routes.write_file(route)
        self.work_with_file_available_seats.check_file()
        list_of_buses = self.work_with_file_buses.read_file()
        for item in list_of_buses:
            if item['bus_id'] == bus_id:
                value = {i for i in range(1, int(item['number_of_seats']) + 1)}
        new_available_seats = {'route_id': route_id, 'seats': value}
        self.work_with_file_available_seats.write_file(new_available_seats)

    def show_buses(self):
        # Отримаємо список автобусів з бази даних
        # ----------------------------------------------------------------------
        if not os.path.exists(os.sep.join([os.getcwd(), 'buses.csv'])) or len(self.work_with_file_buses.read_file()) == 0:
            print("List of buses is empty")
            return
        list_of_buses = []
        for item in self.work_with_file_buses.read_file():
            list_of_buses.append(item.values())
        print(tabulate(list_of_buses, headers=['bus_id', 'model', 'license_plate', 'number_of_seats'],
                       tablefmt='pipe', stralign='center'))

    def show_routes(self):
        # Отримаємо список маршрутів з бази даних
        # ----------------------------------------------------------------------
        if not os.path.exists(os.sep.join([os.getcwd(), 'routes.csv'])) or len(self.work_with_file_routes.read_file()) == 0:
            print("List of routes is empty")
            return
        list_of_routes = []
        for item in self.work_with_file_routes.read_file():
            list_of_routes.append(item.values())
        print(tabulate(list_of_routes, headers=['route_id', 'date', 'time', 'route', 'bus_id'],
                       tablefmt='pipe', stralign='center'))

    def buy_tickets(self):
        # Продаж квитків
        # ----------------------------------------------------------------------
        if not os.path.exists(os.sep.join([os.getcwd(), 'routes.csv'])) or len(self.work_with_file_routes.read_file()) == 0:
            print("List of routes is empty")
            return
        self.routes = []
        list_of_routes = self.work_with_file_routes.read_file()
        for item in list_of_routes:
            new_route = Route(item['route_id'], item['date'], item['time'], item['route'], item['bus_id'])
            self.routes.append(new_route)
        city = input("Enter the name of the city where you'd like to buy a ticket: ")
        routes_to_your_city = [item for item in self.routes
                               if (city in item.route and datetime.strptime(' '.join([item.date, item.time]),
                                                                            '%d-%m-%Y %H:%M') > datetime.now())]
        if routes_to_your_city:
            li = []
            for item in routes_to_your_city:
                print(item)
                li.append(item.route_id)
            route_id = input('Enter chosen Route ID: ')
            try:
                if route_id not in li:
                    raise TicketError("Your choice is wrong")
            except TicketError as err:
                print(err)
                return
        else:
            route_id = random.choice([item.route_id for item in self.routes if
                                      datetime.strptime(' '.join([item.date, item.time]), '%d-%m-%Y %H:%M') >
                                      datetime.now()])
            print(f"We suggest choosing the Route ID: {route_id}")
        chosen_root = [item for item in self.routes if item.route_id == route_id]
        list_of_seats = []
        chosen_seats = []
        self.available_seats = self.work_with_file_available_seats.read_file()
        if not os.path.exists(os.sep.join([os.getcwd(), 'sold_tickets.csv'])):
            self.work_with_file_sold_tickets.create_file()
        else:
            self.sold_tickets = self.work_with_file_sold_tickets.read_file()
        for item in self.available_seats:
            if item['route_id'] == route_id:
                try:
                    list_of_seats = [int(x) for x in re.split(", ", item['seats'][1:-1])]
                    print(f"There are the following available seats on this route: {list_of_seats}")
                    try:
                        chosen_seats = list(map(int, input("Enter the numbers of seats you'd like to buy "
                                                           "separated by a space: ").split()))
                    except ValueError:
                        print("You entered wrong data")
                    if chosen_seats:
                        for seat in chosen_seats:
                            if seat in list_of_seats:
                                ticket_number = chosen_root[0].route[0] + \
                                                chosen_root[0].route[chosen_root[0].route.find('-') + 1] + \
                                                '-' + str(len(self.sold_tickets) + 1).zfill(4)
                                ticket = Ticket(ticket_number, route_id, seat, str(uuid.uuid4()))
                                print(f"Your ticket for the seat № {seat}: ")
                                print(ticket)
                                self.work_with_file_sold_tickets.write_file({'ticket_num': ticket.ticket_num,
                                                                            'route_id': ticket.route_id,
                                                                             'seat': ticket.seat,
                                                                             'uuid': ticket.uuid})
                                self.sold_tickets.append(ticket)
                                list_of_seats.remove(seat)
                            else:
                                print(f"The seat {seat} is not available")
                        chosen_seats.clear()
                    else:
                        print("You haven't chosen any seat")
                    item['seats'] = list_of_seats
                except ValueError:
                    print("Sorry, there are no available seats on this bus route")
        self.work_with_file_available_seats.update_file_with_new_data(self.available_seats)

    def customer_service(self):
        while True:
            try:
                choice = int(input("MENU:\n"
                                   "0 - exit\n"
                                   "1 - show list of routes\n"
                                   "2 - buy ticket\n"
                                   "Make your choice: "))
            except ValueError:
                print("Enter a digit value")
                continue
            match choice:
                case 0:
                    break
                case 1:
                    self.show_routes()
                case 2:
                    self.buy_tickets()
                case _:
                    print("Wrong choice")
