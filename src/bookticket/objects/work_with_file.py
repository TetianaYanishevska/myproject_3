import csv
import os


class WorkWithFile:
    def __init__(self, filepath, fieldnames):
        self.filepath = filepath
        self.fieldnames = fieldnames

    def create_file(self):
        with open(self.filepath, "w", newline='', encoding='utf-8-sig') as csvfile:
            fieldnames = self.fieldnames
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

    def read_file(self):
        data_list = []
        with open(self.filepath, encoding='utf-8-sig') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                data_list.append(row)
        return data_list

    def check_file(self):
        if not os.path.exists(self.filepath):
            self.create_file()

    def write_file(self, data_to_write: dict):
        with open(self.filepath, 'a', newline='', encoding='utf-8-sig') as csvfile:
            fieldnames = self.fieldnames
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow(data_to_write)

    def update_file_with_new_data(self, new_data: list):
        self.create_file()
        for item in new_data:
            self.write_file(item)

